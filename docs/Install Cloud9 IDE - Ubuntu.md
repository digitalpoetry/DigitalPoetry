Install Cloud9 IDE on Ubuntu
======

##### TODO: Complete this guide, it does not work.

*********

Based on the [official installation guide](https://github.com/c9/core/blob/master/README.md)
hosted on Github. Also see this [alternate tutorial](http://ubuntuforums.org/showthread.php?t=1813076)
on Ubuntu Forums.

**Note:** Cloud9 v3 currently requires *Node.js 0.12* or *0.10*. Use
[this tutorial](/docs/Install Node.js - Ubuntu.md) to install node.js.

*********

#### Install Cloud9

Clone the master branch and run setup script:
```ssh
sudo mkdir /var/www
sudo mkdir /var/www/srv
cd /var/www/srv
sudo git clone git://github.com/c9/core.git c9sdk
cd c9sdk
sudo ./scripts/install-sdk.sh
```

*********

#### Update Cloud9

Update to the latest version:
```ssh
cd /var/www/srv/c9sdk
sudo git pull origin master
sudo ./scripts/install-sdk.sh
```

*********

### Usage

Start the Cloud9 as follows:
```ssh
sudo node server.js --help
sudo node --help server.js
sudo node server.js -p 9099 -l 192.168.0.18 -a
./server.js -p 8080 -l 0.0.0.0 -a
```
The following options can be used:

> --settings       # Settings file to use
> --help           # Show command line options
> -t               # Start in test mode
> -k               # Kill tmux server in test mode
> -b               # Start the bridge server - to receive
>                  # commands from the cli  [default: false]
> -w               # Workspace directory
> --port           # Port
> --debug          # Turn debugging on
> --listen         # IP address of the server
> --readonly       # Run in read only mode
> --packed         # Whether to use the packed version
> --auth           # Basic Auth username:password
> --collab         # Whether to enable collab.
> --no-cache       # Don't use the cached version of CSS

Now visit http://localhost:9099/ide.html to load Cloud9.

#### Running Tests

Run server side tests with:
```ssh
npm run test
```
Run client side tests with:
```ssh
npm run ctest
```
Then visit http://c9:9099/static/test in your browser.

*********

Guide by [DigitalPoetry](https://gitlab.com/u/$USER).

#### Cloud9 Resources

<table>
    <tr><th>SDK documentation</th><td>http://cloud9-sdk.readme.io/v0.1/docs</td></tr>
    <tr><th>API documentation</th><td>http://docs.c9.io/api</td></tr>
    <tr><th>User documentation</th><td>http://docs.c9.io</td></tr>
</table>









*********
*********
*********









```sh
    sudo apt-get install apache2
    sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash
    cd c9sdk
    sudo scripts/install-sdk.sh
    sudo node ./scripts/server.js -p 8181 -l 0.0.0.0 -a :
    sudo node server.js -p 8181 -l 0.0.0.0 -a :
```









*********
*********
*********









```
sudo apt-get install apache2

# https://github.com/c9/install
sudo apt-get install nodejs npm
sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash

# https://github.com/c9/core/blob/master/README.md
git clone git://github.com/c9/core.git c9sdk
cd c9sdk
sudo ./scripts/install-sdk.sh

> $USER@DigitalStream:~/c9sdk$ sudo node server.js -p 8181 -l 0.0.0.0 -a :
> Connect server listening at http://192.168.0.18:8181
> CDN: version standalone initialized /home/$USER/c9sdk/build
> Started '/home/$USER/c9sdk/configs/standalone' with config 'standalone'!

sudo node server.js -p 8181 -l 0.0.0.0 -a :
```




# TEMINAL HISTORY:
```sh
1  sudo apt-get update && sudo apt-get upgrade
2  apt list python
3  sudo apt-get install git
4  ls
5  ls -alh
6  git clone https://github.com/c9/core.git c9
7  cd c9
8  ./scripts/install-sdk.sh
9  sudo ./scripts/install-sdk.sh
10  sudo apt-get install build-essential
11  sudo ./scripts/install-sdk.sh
12  cat ./scripts/install-sdk.sh
13  #!/bin/bash -e
14  cd `dirname $0`/..
15  SOURCE=`pwd`
16  uname="$(uname -a)"
17  os=
18  arch="$(uname -m)"
19  case "$uname" in     Linux\ *) os=linux ;;     Darwin\ *) os=darwin ;;     SunOS\ *) os=sunos ;;     FreeBSD\ *) os=freebsd ;;     CYGWIN*) os=windows ;;     MINGW*) os=windows ;;     MSYS_NT*) os=windows ;; esac
20  case "$uname" in     *x86_64*) arch=x64 ;;     *i*86*) arch=x86 ;;     *armv6l*) arch=arm-pi ;;     *armv7l*) arch=arm-pi ;; esac
21  red=$'\e[01;31m'
22  green=$'\e[01;32m'
23  yellow=$'\e[01;33m'
24  blue=$'\e[01;34m'
25  magenta=$'\e[01;35m'
26  resetColor=$'\e[0m'
27  NO_PULL=
28  NO_GLOBAL_INSTALL=
29  FORCE=
30  updatePackage() {     name=$1;      REPO=https://github.com/c9/$name;     echo "${green}checking out ${resetColor}$REPO";      if ! [[ -d ./plugins/$name ]]; then         mkdir -p ./plugins/$name;     fi;      pushd ./plugins/$name;     if ! [[ -d .git ]]; then         git init         git remote add origin $REPO;     fi;      version=`"$NODE" -e 'console.log((require("../../package.json").c9plugins["'$name'"].substr(1) || "origin/master"))'`;     rev=`git rev-parse --revs-only $version`;      if [ "$rev" == "" ]; then         git fetch origin;     fi;      status=`git status --porcelain --untracked-files=no`;     if [ "$status" == "" ]; then         git reset $version --hard;     else         echo "${yellow}$name ${red}contains uncommited changes.${yellow} Skipping...${resetColor}";     fi;     popd; }
31  updateAllPackages() {     c9packages=(`"$NODE" -e 'console.log(Object.keys(require("./package.json").c9plugins).join(" "))'`);     count=${#c9packages[@]};     i=0;     for m in ${c9packages[@]}; do echo $m;         i=$(($i + 1));         echo "updating plugin ${blue}$i${resetColor} of ${blue}$count${resetColor}";         updatePackage $m;     done; }
32  updateNodeModules() {     echo "${magenta}--- Running npm install --------------------------------------------${resetColor}";     safeInstall(){         deps=(`"$NODE" -e 'console.log(Object.keys(require("./package.json").dependencies).join(" "))'`);         for m in $deps; do echo $m;             "$NPM" install --loglevel warn $m || true;         done;     };     "$NPM" install || safeInstall;     echo "${magenta}--------------------------------------------------------------------${resetColor}"; }
33  updateCore() {     if [ "$NO_PULL" ]; then         return 0;     fi;      mv ./scripts/install-sdk.sh  ./scripts/.install-sdk-tmp.sh;     cp ./scripts/.install-sdk-tmp.sh ./scripts/install-sdk.sh;     git checkout -- ./scripts/install-sdk.sh;      git remote add c9 https://github.com/c9/core 2> /dev/null || true;     git fetch c9;     git merge c9/master --ff-only ||         echo "${yellow}Couldn't automatically update sdk core ${resetColor}";  }
34  installGlobalDeps() {     if ! [[ -f ~/.c9/installed ]] && ! [[ $os == "windows" ]]; then         curl https://raw.githubusercontent.com/c9/install/master/install.sh | bash;     fi; }
35  ############################################################################
36  NPM=npm
37  NODE=node
38  # cleanup build cache since c9.static doesn't do this automatically yet
39  rm -rf ./build/standalone
40  # pull the latest version
41  updateCore || true
42  installGlobalDeps
43  updateAllPackages
44  updateNodeModules
45  echo "Success!"
46  echo "run '${yellow}node server.js -p 8181 -l 0.0.0.0 -a :${resetColor}' to launch Cloud9"
47  wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | bash
48  sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash
49  ls
50  sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash help
51  sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash --help
52  sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh
53  sudo wget - https://raw.githubusercontent.com/c9/install/master/install.sh
54  sudo wget https://raw.githubusercontent.com/c9/install/master/install.sh
55  ls
56  install.sh help
57  sudo install.sh help
58  ./server.js -p 8080 -l 192.168.0.18 -a :
59  cd c9
60  cd .c9
61  ls -alh
62  cd ~
63  ls -alh
64  cd cp
65  cd c9
66  ls -alh
67  ls scripts
68  cd scripts
69  nano makestandalone.sh
70  nano setup-local-dev.sh
71  nano makelocal.sh
72  cd ..
73  ls
74  ./server.js -p 8080 -l 0.0.0.0 -a :
75  git clone https://github.com/c9/core.git c9
76  cd ~
77  rm -R ~/c9
78  ls
79  ls -alh
80  sudo rm -R .c9
81  ls -alh
82  git clone https://github.com/c9/core.git c93
83  git clone https://github.com/c9/core.git c9
84  sudo rm -R .c93
85  ls -alh
86  sudo rm -R c93
87  cd c9
88  cat ./scripts/install-sdk.sh
89  sudo ./scripts/install-sdk.sh
90  ./server.js -p 8080 -l 0.0.0.0 -a
91  sudo ./server.js -p 8080 -l 0.0.0.0 -a
92  ln -s /usr/bin/nodejs /usr/bin/node
93  sudo ln -s /usr/bin/nodejs /usr/bin/node
94  sudo ./server.js -p 8080 -l 0.0.0.0 -a
95  sudo nodejs ./server.js -p 8080 -l 0.0.0.0 -a
96  sudo node ./server.js -p 8080 -l 0.0.0.0 -a
97  node server.js
98  sudo apt show node
99  sudo apt-get remove node
100  sudo apt show nodejs
101  sudo apt-get remove nodejs
102  sudo apt-get install nodejs
103* sudo ln -s /usr/bin/nodejs /usr/bin/nodejs
104  sudo node ./server.js -p 8080 -l 0.0.0.0 -a
105  sudo node ./server.js -p 8080 -l 0.0.0.0 -a :
106  sudo node ./server.js -p 8080 -l 192.168.0.18 -a :
107  sudo rm -R .c9
108  cd ~
109  sudo rm -R .c9
110  sudo rm -R c9
111  sudo wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | sudo bash
112  apt list python
113  sudo node ./server.js -p 8080 -l 0.0.0.0 -a
114  cd c9
115  cd .c9
116  sudo node ./server.js -p 8080 -l 0.0.0.0 -a
117* git clone https://github.com/c9/core.git c9sdkcd
118  sudo git clone https://github.com/c9/core.git c9sdk
119  cd c9sdk
120  ./scripts/install-sdk.sh
121  sudo ./scripts/install-sdk.sh
122  node server.js -p 8181 -l 0.0.0.0 -a :
123  sudo node server.js -p 8181 -l 0.0.0.0 -a :
124  sudo node server.js -p 8181 -l 0.0.0.0 -a :cd ~
125  npm install -g codebox
126  sudo apt-get install npm
127  ls
128  cd ~
129  sudo rm -R .c9
130  sudo rm -R c9
```
