DigitalPoetry Development Setup
===

*Set up some AHK expansions to make all of theses easier???*

Composer
------------
- "bmitch/churn-php:""dev-master"
- "dephpend/dephpend":"dev-master"
- "edsonmedina/php_testability":"dev-master"
- "phan/phan":"1.x"
+ "phpdocumentor/phpdocumentor":"2.*"
- "phpunit/phpunit":"^7"
+ "squizlabs/php_codesniffer":"3.*"
- "wapmorgan/php-code-analyzer":"dev-master"
- "wapmorgan/php-code-fixer":"dev-master"
- "block8/phpci"
- "codeception/codeception"
- "CSSLint/csslint"
+ "friendsofphp/php-cs-fixer"
- "infection/infection"
- "jakub-onderka/php-parallel-lint"
- "m6web/php-lint"
- "markdownlint/markdownlint"
- "niels-de-blaauw/php-doc-check"
- "nikic/php-parser"
+ "odan/docblock-checker"
- "pdepend/pdepend"
- "phpactor/phpactor"
- "phpcompatibility/php-compatibility"
+ "phploc/phploc"
- "phpmd/phpmd"
- "phpmetrics/phpmetrics"
- "phpspec/phpspec"
+ "phpstan/phpstan"
- "phpstan/phpstan-php-parser"
- "phpunit/php-code-coverage"
- "povils/phpmnd"
- "QafooLabs/php-refactoring-browser"
- "require nikic/php-parser"
- "rskuipers/php-assumptions"
- "sebastian/phpcpd"
+ "sebastian/phpdcd"


ST3 Packages
------------
- A File Icon
- Alignment
- AutoFileName
- BracketHighlighter
- Browser Refresh
- Clipboard Manager
- CodeIgniter Utilities
- Color Highlight
- CSS3
- DocBlockr with comments update
- FindKeyConflicts
- HTML5
- JSONLint
- LESS
- Line Endings Unify
- MarkdownLight
- MarkdownPreview
- Material Theme
- Monokai - Spacegray
- Monokai Extended
- Origami
- Outline
- Package Control
- PackageDev
- PHP Completions Kit
- SCSS
- Search Anywhere
- SideBarEnhancements
- subl protocol
- SublimeCodeIntel
- SublimeLinter
- SublimeLinter-csslint
- SublimeLinter-jshint
- SublimeLinter-json
- Sublimerge 3
- Terminus
- Theme - Monokai Pro
- Theme - Spacegray
- Toggle Words
- Xdebug Client

PreReq's
----------
- Git
- Composer
- Node.js LTS
    - NPM install
        - CSSLint
        - JSHint
- Python 2.6?
    - PIP Install
        - CodeIntel