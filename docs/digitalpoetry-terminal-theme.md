Terminal Color Scheme
---

```
Ansi color X:   R,    G,    B
-------------------------------
Ansi color 0:   0,    0,    8
Ansi color 1:   0,    115,  215
Ansi color 2:   0,    204,  91
Ansi color 3:   0,    199,  199
Ansi color 4:   215,  8,    17
Ansi color 5:   106,  66,   215
Ansi color 6:   205,  169,  0
Ansi color 7:   209,  209,  215
Ansi color 8:   20,   20,   48
Ansi color 9:   20,   155,  255
Ansi color 10:  20,   244,  131
Ansi color 11:  20,   239,  239
Ansi color 12:  255,  48,   57
Ansi color 13:  146,  106,  255
Ansi color 14:  245,  209,  20
Ansi color 15:  249,  249,  255
-------------------------------
Opacity: 85% - 90%
```