Install Cloud9 & Cloud9Hub on Ubuntu
===

Node.js
---
First you will need node.js, at least v0.8. Use [this tutorial](/docs/Install Node.js - Ubuntu.md) to install node.js.

Quick install script:
```ssh
    curl https://raw.githubusercontent.com/AVGP/cloud9hub/master/install.sh | sh
```
This should install Cloud9 into the working directory.

Configuration
---
First things first: You need a Github application to provide the "Login with
Github" feature, which is currently the only login mechanism.

Go to [GitHub](https://github.com/settings/applications/new) and create a new
application. Note down the client ID and secret, you'll need them later.

Now copy the `config.js.example` to `config.js` and edit the contents.
 + Add your Github client ID and secret
 + Change your BASE_URL to your server's address (do not include the port!)

Copy & Edit Configuration:
```ssh
cat cloud9hub/config.js.example > cloud9hub/config.js
sudo nano cloud9hub/config.js
```
Firewall
---
You will need ports 3000 and 5000 to however many connections will be taking
place concurrently (each session is given a different port).

Disable ufw:
```ssh
sudo ufw disable
```
Running as a daemon
---
If you wish to, you can run it as a daemon, so that it stays alive.

To do so, I recommend [forever](https://npmjs.org/package/forever).
