# Todo
- Make function to replace `defined('BASEPATH') OR exit('No direct script access allowed');`
- DB Sessions by default
- https://remysharp.com/2018/08/23/cli-improved
- https://github.com/nojhan/liquidprompt
- Make ST3 build scripts:
    - PHP lint file
    - PHP exec file
    - PHP lint Project
    - PHP exec Project
    - QA tools Builds
- Fork [OnSaveBuildPlugin]() 
- [Setup](https://github.com/dseguy/clearPHP/blob/master/rules/README.md)
- [Setup](https://www.exakat.io/phplint/about.php)
- [Setup](https://github.com/phpstan/)
- [Setup](https://github.com/nikic/php-parser)
- [Setup](http://www.program-transformation.org/PHP/PhpSat)
- [Setup](https://github.com/twbs/bootlint)
- [Setup](https://github.com/phan/phan)
- [Exakat](https://exakat.readthedocs.io/en/latest/Installation.html#installation-guide-with-composer)
- [PHPCI](https://docs.phptesting.org/en/latest/installing-phpci/)
- [Code Standards](https://phpqa.io/projects/php-compatibility.html)
- [Useful?](https://github.com/FriendsOfPHP/PHP-CS-Fixer/tree/2.14/dev-tools)
- [DocBlocks Cheat Sheet](http://phpdoc2cheatsheet.com/#class)
- Docker?

# Misc
```bash
# Set PHP verson w/ apache
sudo a2dismod php7.1
sudo a2enmod php7.3
sudo service apache2 restart
# OR
sudo a2dismod php7.1
sudo a2enmod php7.3
sudo service apache2 restart

# Set PHP verson w/ commandline
sudo update-alternatives --config php

#  Install composer suggests
composer suggests | xargs -i composer require {}
```

```
find . -type f  -exec touch {} +
```







OLD Unsorted Tutorial Snippets
=========

<!-- MarkdownTOC -->

- [IDE's](#ides)
- [GIT SCM](#git-scm)
- [Self Host Gitlab CE](#self-host-gitlab-ce)
- [Review GitHub Pages](#review-github-pages)
- [phpSysInfo](#phpsysinfo)

<!-- /MarkdownTOC -->


IDE's
---
Sitepoint's [5 Cloud IDE's reviewed](sitepoint.com/sitepoint-smackdown-atom-vs-brackets-vs-light-table-vs-sublime-text) is informational.

*Cloud9* followed by *Codebox* look like the best 2 free IDE's.

**Cloud9 Self Host.**
[Installation](https://github.com/c9/core/blob/master/README.md). See [Cloud9](https://github.com/c9/core/) & [Cloud9Hub](zurb.com/forrst/posts/Cloud9Hub_multi_workspace_self_hosted_Cloud9-FZr)
for information.

**Codebox IDE Self Host.**
See [Codebox](https://www.codebox.io/stack/php) for information. Installation guide [here](https://github.com/CodeboxIDE/codebox).

**DevDocs <3.**
See [DevDocs](http://devdocs.io/about#plugins) for information.

**Jekyll.** Transforms plain text into static websites and blogs. [Website](jekyllrb.com/).

*********
