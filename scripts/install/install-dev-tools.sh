#!/bin/bash
# Usage: $ sudo install-dev-tools.sh


# Check the line ending style of the scipts. Stop the script if not unix style.
file install_tools.sh | grep -q CRLF && echo "This file has Windows style " \
"line endings (CRLF). Please correct the file and run it again. This script " \
"will now end." && exit


# Set vars for colorizing output
NC='\033[0m'      # No Color
DGRY='\033[1;30m' # Dark Gray
YEL='\033[1;33m'  # Yellow
PUR='\033[0;35m'  # Purple
CYN='\033[0;36m'  # Cyan
LCYN='\033[1;36m' # Light Cyan


# Instructions / Get windows user profile path
echo -e "
${CYN}============================
${LCYN}PHP Dev Tools Installer
${CYN}============================
${NC}
This script installs php dev tools for Windows 10 with WSL.
${NC}
${YEL}
NOTE: You must run this script as sudo!!!
${PUR}
$ sudo bash install-dev-tools.sh
${NC}"


# Set the working directory
cd ~/.devtools


# Install Tools
echo -e "
${CYN}============================
${LCYN}Installing PHP Dev Tools
${CYN}============================
${YEL}
Installing to `pwd`
${NC}"


# CSSLint & JSHint via NPM
npm install -g csslint
npm install -g jshint
npm install -g markdownlint-cli


# Create directories & require package with php composer...
mkdir churn && cd churn && composer require -n "bmitch/churn-php":"dev-master"
cd ~/.devtools
mkdir codeception && cd codeception && composer require -n "codeception/codeception"
cd ~/.devtools
mkdir dephpend && cd dephpend && composer require -n "dephpend/dephpend"
cd ~/.devtools
mkdir docblock_checker && cd docblock_checker && composer require -n "odan/docblock-checker"
cd ~/.devtools
mkdir infection && cd infection && composer require -n "infection/infection"
cd ~/.devtools
mkdir pdepend && cd pdepend && composer require -n "pdepend/pdepend"
cd ~/.devtools
mkdir phan && cd phan && composer require -n "phan/phan":"1.x"
cd ~/.devtools
mkdir php_assumptions && cd php_assumptions && composer require -n "rskuipers/php-assumptions"
cd ~/.devtools
mkdir php_code_analyzer && cd php_code_analyzer && composer require -n "wapmorgan/php-code-analyzer":"dev-master"
cd ~/.devtools
mkdir php_code_coverage && cd php_code_coverage && composer require -n "phpunit/php-code-coverage"
cd ~/.devtools
mkdir php_code_fixer && cd php_code_fixer && composer require -n "wapmorgan/php-code-fixer":"dev-master"
cd ~/.devtools
mkdir php_codesniffer && cd php_codesniffer && composer require -n "squizlabs/php_codesniffer":"3.*"
cd ~/.devtools
mkdir php_compatibility && cd php_compatibility && composer require -n "phpcompatibility/php-compatibility"
cd ~/.devtools
mkdir php_cs_fixer && cd php_cs_fixer && composer require -n "friendsofphp/php-cs-fixer"
cd ~/.devtools
mkdir php_doc_check && cd php_doc_check && composer require -n "niels-de-blaauw/php-doc-check"
cd ~/.devtools
mkdir php_lint && cd php_lint && composer require -n "m6web/php-lint"
cd ~/.devtools
mkdir php_parallel_lint && cd php_parallel_lint && composer require -n "jakub-onderka/php-parallel-lint"
cd ~/.devtools
mkdir php_parser && cd php_parser && composer require -n "nikic/php-parser"
cd ~/.devtools
mkdir php_parser && cd php_parser && composer require -n "require nikic/php-parser"
cd ~/.devtools
mkdir php_refactoring_browser && cd php_refactoring_browser && composer require -n "QafooLabs/php-refactoring-browser"
cd ~/.devtools
mkdir php_testability && cd php_testability && composer require -n "edsonmedina/php_testability":"dev-master"
cd ~/.devtools
mkdir phpactor && cd phpactor && composer require -n "sebastian/diff":"2.0.x-dev" && composer require -n "phpactor/path-finder":"dev-master" && composer require -n "phpactor/phpactor"
cd ~/.devtools
mkdir phpci && cd phpci && composer require -n "block8/phpci"
cd ~/.devtools
mkdir phpcpd && cd phpcpd && composer require -n "sebastian/phpcpd"
cd ~/.devtools
mkdir phpdCD && cd phpdCD && composer require -n "sebastian/phpdcd"
cd ~/.devtools
mkdir phpdocumentor && cd phpdocumentor && composer require -n "phpdocumentor/phpdocumentor":"2.*"
cd ~/.devtools
mkdir phploc && cd phploc && composer require -n "phploc/phploc"
cd ~/.devtools
mkdir phpmd && cd phpmd && composer require -n "phpmd/phpmd"
cd ~/.devtools
mkdir phpmetrics && cd phpmetrics && composer require -n "phpmetrics/phpmetrics"
cd ~/.devtools
mkdir phpmnd && cd phpmnd && composer require -n "povils/phpmnd"
cd ~/.devtools
mkdir phpspec && cd phpspec && composer require -n "phpspec/phpspec"
cd ~/.devtools
mkdir phpstan && cd phpstan && composer require -n "phpstan/phpstan"
cd ~/.devtools
mkdir phpstan_php_parser && cd phpstan_php_parser && composer require -n "phpstan/phpstan-php-parser"
cd ~/.devtools
mkdir phpunit && cd phpunit && composer require -n "phpunit/phpunit":"^7"
cd ~/.devtools
cd ~/scripts/install

echo -e "
${CYN}============================${NC}
Installation complete!

"