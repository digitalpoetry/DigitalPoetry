#!/bin/bash
# Built on Ubuntu 18.04 Bionic
# Usage: $ sudo bash install-wsl-lamp.sh


# Check the line ending style of the scipts. Stop the script if not unix style.
file install_tools.sh | grep -q CRLF && echo "This file has Windows style " \
"line endings (CRLF). Please correct the file and run it again. This script " \
"will now end." && exit


# Set vars for colorizing output
NC='\033[0m'      # No Color
DGRY='\033[1;30m' # Dark Gray
YEL='\033[1;33m'  # Yellow
PUR='\033[0;35m'  # Purple
CYN='\033[0;36m'  # Cyan
LCYN='\033[1;36m' # Light Cyan


# Instructions / Get windows user profile path
echo -e "
${CYN}============================
${LCYN}WSL LAMP Installer
${CYN}============================
${NC}
This script installs a LAMP stack with PHPMyAdmin, and a simple Hosting Control Panel on WSl for Windows 10.
${YEL}
NOTE: You must run this script as sudo!!!
${PUR}
$ sudo bash install-wsl-lamp.sh
${NC}
Enter your Development directory path below.
NOTE: Backslashes must be escaped! For example:
${PUR}
    Windows Path   > Escaped Path
${NC}"
echo -E  "    C:\DigitalPoetry > C:\\\DigitalPoetry

"
read -p "[C:\\\DigitalPoetry]: " PROFILE


# Set a default path and convet to linx
PROFILE=${PROFILE:-"C:\\DigitalPoetry"}
PROFILE=$(bash -c "wslpath '${PROFILE}'")


# Verify user profile path or exit script
echo -e "Is the following WSL path is correct:
${YEL}
${PROFILE}
${NC}"
read -p "(Y/n)?" CHOICE
# Set a default CHOICE
CHOICE=${CHOICE:-"Y"}
case "$CHOICE" in
  Y|y ) echo -e "
${CYN}============================
${LCYN}
Setting WSL path to ${PROFILE}
";;
  * ) read -p "============================
${YEL}
Unable to set Home directory. Please run installation script again.
${NC}
Press enter to exit...
"; exit;;
esac


# Verify Profile path exists
if [ ! -d ${PROFILE} ]
then
    read -p "============================
${YEL}
User Profile directory does not exist. Please correct issue and run installation script again.
${NC}
Press enter to exit...
"
    exit
fi


# Disable sudoer timeouts
echo -e "
${CYN}============================
${PUR}
# Diasble sudoer timeouts
Defaults:digitalpoetry    !authenticate
${NC}
Append the above to the soduers file:
${YEL}
$ sudo visudo
${PUR}
Skip this step if already completed
${NC}"
read -p "Hit enter when (or if already) completed...
"


# Root user 'Home' directory
echo -e "
${CYN}============================
${PUR}
${PROFILE}
${NC}
Set user Home directory to the above, replacing:
${PUR}
$HOME
${YEL}
$ sudo vi /etc/passwd
${PUR}
Reboot WSL Now and re-run this scipt.
Open Command Promt and run the following:
${YEL}
net stop LxssManager
net start LxssManager
${NC}"
read -p "Hit enter when (or if already) completed...
"


# Set DigitalPoetry path permissions
chmod -R 755 "${PROFILE}"


# Updating Unbuntu
echo -e "
${CYN}============================
${LCYN}Update Unbuntu
${CYN}============================
${NC}"
apt -y update
apt -y upgrade


# Adding PPAs
echo -e "
${CYN}============================
${LCYN}Add PPAs
${CYN}============================
${NC}"
add-apt-repository -y ppa:ondrej/apache2
add-apt-repository -y ppa:ondrej/php
apt -y update


# Installing Apache2
echo -e "
${CYN}============================
${LCYN}Install Apache2
${CYN}============================
${NC}"
apt -y install apache2
service apache2 start
a2enmod rewrite
a2enmod vhost_alias


# Fix error in WSL (https://github.com/Microsoft/WSL/issues/1953#issuecomment-295370994)
echo -e "
${CYN}============================
${PUR}
# Fix error in WSL (https://github.com/Microsoft/WSL/issues/1953#issuecomment-295370994)
AcceptFilter http none
AcceptFilter https none
${NC}
Append the above to apache2.conf:
${YEL}
$ sudo vi /etc/apache2/apache2.conf
${NC}"
read -p "Hit enter when completed...
"
service apache2 restart


# Default Virtualhost
echo -e "
${CYN}============================
${PUR}
# Dev Host Dashboard
<VirtualHost *:80>
    # Needed for locally spoffed IP's
    UseCanonicalName Off
    # Set the virtualhost FQDN or hostname
    ServerName localhost
    ServerAlias digitalpoetry
    # Set the virtualhost document root
    DocumentRoot ${PROFILE}/localhost
    # Set the virtualhost log paths
    ErrorLog ${PROFILE}/localhost/log/error.log
    CustomLog ${PROFILE}/localhost/log/access.log combined
    # Set the virtualhost directory options
    <Directory ${PROFILE}/localhost>
        # Grant all, or grant to a specific host. ie: Require host example.org
        Require all granted
    </Directory>
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
${NC}
Use the above to replace the contents of 000-default.conf:
${YEL}
$ sudo vi /etc/apache2/sites-available/000-default.conf
${NC}"
read -p "Hit enter when completed...
"


# Automatic Virtualhost
echo -e "
${CYN}============================
${PUR}
# Automatic Vhosts
<Virtualhost *:80>
    # Needed for locally spoffed IP's
    UseCanonicalName Off
    # Set the virtualhost FQDN or hostname
    ServerAlias *
    # Set the virtualhost document root
    VirtualDocumentRoot ${PROFILE}/localhost/www/%0/public_html
    VirtualScriptAlias ${PROFILE}/localhost/www/%0/public_html/cgi-bin
    # Set the virtualhost directory options
    <Directory ${PROFILE}/localhost/www/*>
        # Set the directory options
        Options Indexes FollowSymLinks
        # Allow .htaccess overrides
        AllowOverride All
        # Grant all, or grant to a specific host. ie: Require host example.org
        Require all granted
    </Directory>
</Virtualhost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
${NC}
Add the above to automatic-vhosts.conf:
${YEL}
$ sudo vi /etc/apache2/sites-available/automatic-vhosts.conf
${NC}"
read -p "Hit enter when completed...
"


# Enable virtualhosts
echo -e "
${CYN}============================
${LCYN}Enabling virtualhosts
${CYN}============================
${NC}"
a2ensite automatic-vhosts.conf
service apache2 reload


# Installing MySQL
echo -e "
${CYN}============================
${LCYN}Install MySQL
${CYN}============================
${NC}"
apt -y install mysql-server

# Set the MySQL user home directory to suppress warnings
usermod -d /var/lib/mysql mysql

# Start MySQL and secure intallation
service mysql start


# Install PHP & extentions
echo -e "
${CYN}============================
${LCYN}Installing PHP & extentions
${CYN}============================
${NC}"
apt -y install php7.1 php7.1-bz2 php7.1-common php7.1-curl php7.1-gd php7.1-gettext php7.1-mbstring php7.1-mcrypt php7.1-mysql php7.1-xml php7.1-zip
phpenmod mbstring


# Install dev tools
echo -e "
${CYN}============================
${LCYN}Installing dev tools
${CYN}============================
${NC}"
apt -y install php-pear git composer jsonlint
git config --global user.email "jlareaux@gmail.com"
git config --global user.name "Jesse LaReaux"
git config --global core.autocrlf input


# Install Node.js
echo -e "
${CYN}============================
${LCYN}Installing Node.js & Gulp
${CYN}============================
${NC}"
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt -y install nodejs
npm install -g gulp


# Install PHPMyAdmin
echo -e "
${CYN}============================
${LCYN}Installing PHPMyAdmin
${CYN}============================
${NC}"
apt -y install phpmyadmin
service apache2 start
echo -e "
${CYN}============================
RUN:
${PUR}
$ sudo mysql -u root -p
> GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
> FLUSH PRIVILEGES;
> exit;
${NC}"
read -p "Hit enter when completed...
"


# Configure PHPMyAdmin for no password at login
echo -e "
${CYN}============================
${PUR}
    \$cfg['Servers'][\$i]['auth_type'] = 'config';
    \$cfg['Servers'][\$i]['user'] = 'phpmyadmin';
    \$cfg['Servers'][\$i]['password'] = 'lj-=0491';
${NC}
Add the above to config.inc.php, replacing:
${PUR}
\$cfg['Servers'][\$i]['auth_type'] = 'cookie';
${NC}
Be sure to to change ${PUR}lj-=0491${NC} to your phpmyadmin MySQL user password
${YEL}
$ sudo vi /etc/phpmyadmin/config.inc.php
${NC}"
read -p "Hit enter when completed...
"


# Enable the PHPMyAdmin virtualhost
echo -e "
${CYN}============================
${PUR}
# Enable the PHPMyAdmin virtualhost
Include /etc/phpmyadmin/apache.conf
${NC}
Append the above to apache2.conf.
Add before sites are included:
${YEL}
$ sudo vi /etc/apache2/apache2.conf
${NC}"
read -p "Hit enter when completed...
"


# Restart services
echo -e "
${CYN}============================
${LCYN}Restarting Apache & MySQL
${CYN}============================
${NC}"
service apache2 restart
service mysql restart


# XDebug
echo -e "
${CYN}============================
${LCYN}Building XDebug
${CYN}============================
${NC}"
apt -y install php7.1-dev automake autoconf
wget -O xdebug-2.5.5.tar.gz https://github.com/xdebug/xdebug/archive/XDEBUG_2_5_5.tar.gz
tar -xvzf xdebug-2.5.5.tar.gz
cd xdebug-XDEBUG_2_5_5
phpize
./configure --enable-xdebug
make clean
make
make install
cp modules/xdebug.so /usr/lib/php/20160303
chmod 777 /usr/lib/php/20160303/xdebug.so
cd ..
rm -rf xdebug-XDEBUG_2_5_5
rm -f xdebug-2.5.5.tar.gz
apache2ctl restart
echo -e "
${CYN}============================
${PUR}
[xdebug]
zend_extension="/usr/lib/php/20160303/xdebug.so"
xdebug.remote_enable = 1
xdebug.remote_host = "127.0.0.1"
xdebug.remote_port = 9000
xdebug.remote_handler = "dbgp"
xdebug.remote_mode = req
xdebug.file_link_format = "subl://%f:%l"
xdebug.profiler_enable = Off
xdebug.profiler_enable_trigger = Off
xdebug.profiler_output_name = cachegrind.out.%t.%p
xdebug.profiler_output_dir ="/tmp/xdebug"
xdebug.show_local_vars = 1
xdebug.collect_assignments = 1
xdebug.collect_includes = 1
xdebug.collect_params = 1
xdebug.collect_return = 1
xdebug.collect_vars = 1
xdebug.extended_info = 1
${NC}
Append the above to the php.ini:
${YEL}
$ sudo vi /etc/php/7.1/apache2/php.ini
${NC}"
read -p "Hit enter when completed...
"


# OPCache
echo -e "
${CYN}============================
${LCYN}Disable OPCache
${CYN}============================
Find the below line in the opcache.ini file and comment it out:
${PUR}
;zend_extension=opcache.so
${YEL}
$ sudo vi /etc/php/7.1/mods-available/opcache.ini
${NC}"
read -p "Hit enter when completed...
"


# Start apache when wsl is open
echo -e "
${CYN}============================
${PUR}
%sudo ALL=(root) NOPASSWD: /usr/sbin/service *
%wheel ALL=(root) NOPASSWD: /usr/sbin/service *
${NC}
Append the above to the sudoers file:
${YEL}
$ sudo visudo -f /etc/sudoers.d/services
${NC}
Then run the following command in Windows Command Prompt (cmd.exe) as an administrator:
${YEL}
SchTasks /Create /SC ONLOGON /TN \"Start WSL LAMP\" /TR \"c:\\Windows\\System32\\\bash.exe -c 'sudo service apache2 start; sudo service mysql start; cd ~; bash'\"
${NC}"
read -p "Hit enter when completed...
"


# PHP Manual
echo -e "
${CYN}============================
${LCYN}Installing PHP Manual
${CYN}============================
${NC}"
# wget https://www.php.net/distributions/manual/php_manual_en.tar.gz
wget https://www.php.net/distributions/manual/php_manual_en.html.gz
gzip -d php_manual_en.html.gz
mkdir /etc/php/7.1/manual/
cp php_manual_en.html /etc/php/7.1/manual/php_manual_en.html
rm php_manual_en.html


# Bash Utilities
echo -e "
${CYN}============================
${LCYN}Installing Bash Utilities
${CYN}============================
${NC}"
chown -R $USER:$USER $PROFILE
chmod -R 755 $PROFILE
mkdir ${PROFILE}/.bash
cp ${PROFILE}/scripts/install/templates/.bash_profile ${PROFILE}/.bash_profile
cp ${PROFILE}/scripts/install/templates/git_prompt.sh ${PROFILE}/.bash/git_prompt.sh
apt install xtitle

# Alias Me
echo -e "
${CYN}============================
${NC}Alias Me (https://github.com/Jintin/aliasme)
Usage:
${PUR}$ al -h
${NC}"
mkdir ${PROFILE}/.aliasme
wget https://raw.githubusercontent.com/Jintin/aliasme/master/aliasme.sh -O ${PROFILE}/.aliasme/aliasme.sh && \
echo "
# Alias Me (https://github.com/Jintin/aliasme)
source .aliasme/aliasme.sh" >> ${PROFILE}/.bash_profile


# The Fuck
echo -e "
${CYN}============================
${NC}The Fuck (https://github.com/nvbn/thefuck)
Usage:
${PUR}$ apt install php
${DGRY}E: Could not open lock file...
${PUR}$ fuck
  ${DGRY}*magic*
${PUR}$ sudo apt install php
${NC}To run fixed commands without confirmation, use the --yeah option (or just -y for short):
${PUR}$ fuck --yeah
${NC}To fix commands recursively until succeeding, use the -r option:
${PUR}$ fuck -r
${NC}"
mkdir ${PROFILE}/.cache
mkdir ${PROFILE}/.cache/pip
mkdir ${PROFILE}/.cache/pip/http
chown -R $USER:$USER ${PROFILE}/.cache
chmod -R 755 ${PROFILE}/.cache
apt -y install python3-dev python3-pip python3-setuptools
pip3 install thefuck && \
echo "
# The Fuck (https://github.com/nvbn/thefuck)
alias fuck=\"thefuck\"
alias fk=\"thefuck\"" >> ${PROFILE}/.bash_profile


# Hollywood Hacker Screen
echo -e "
${CYN}============================
${NC}Hollywood (https://itsfoss.com/hollywood-hacker-screen/)
Usage:
${PUR}$ hollywood
${NC}"
apt-add-repository -y ppa:hollywood/ppa
apt -y update
apt -y install byobu hollywood && \
echo "
# Hollywood (https://itsfoss.com/hollywood-hacker-screen/)
alias haxor=\"hollywood\"
alias 1337=\"hollywood\"" >> ${PROFILE}/.bash_profile


# HSTR history suggest box
echo -e "
${CYN}============================
${NC}HSTR (https://github.com/dvorka/hstr)
Usage:
${PUR}$ hstr
${NC}"
add-apt-repository -y ppa:ultradvorka/ppa
apt -y update
apt -y install hstr && \
echo -n "
# HSTR (https://github.com/dvorka/hstr)" >> ${PROFILE}/.bash_profile
hstr --show-configuration >> ${PROFILE}/.bash_profile


# PHP.INI
echo -e "
${CYN}============================
${PUR}
expose_php = On
max_execution_time = 300
max_input_time = 120
memory_limit = 1024M
display_errors = On
display_startup_errors = On
log_errors = Off
track_errors = On
docref_root = \"/etc/php/7.1/apache2/manual/\"
docref_ext = .html
upload_max_filesize = 32M
extension=php_bz2.dll
extension=php_curl.dll
extension=php_fileinfo.dll
extension=php_gd2.dll
extension=php_gettext.dll
extension=php_gmp.dll
extension=php_intl.dll
extension=php_mbstring.dll
extension=php_exif.dll ; Must be after mbstring as it depends on it
extension=php_mysqli.dll
extension=php_odbc.dll
extension=php_openssl.dll
extension=php_pdo_mysql.dll
extension=php_pdo_odbc.dll
extension=php_pdo_sqlite.dll
extension=php_pdo_sqlite.dll
extension=php_soap.dll
extension=php_sockets.dll
extension=php_sqlite3.dll
extension=php_tidy.dll
extension=php_xmlrpc.dll
extension=php_xsl.dll
date.timezone = \"America/Phoenix\"
mysqli.default_host = \"localhost\"
mysqli.default_user = \"phpmyadmin\"
mysqli.default_pw = \"lj-=0491\"
opcache.enable=0
${NC}
Modify php.ini to match the above:
${YEL}
$ sudo vi /etc/php/7.1/apache2/php.ini
${NC}"
read -p "Hit enter when completed....
"
service apache2 reload


# Make th Windows hosts file editable
chown $USER:$USER /mnt/c/Windows/System32/drivers/etc/hosts
chmod 644 /mnt/c/Windows/System32/drivers/etc/hosts


# Script information
echo -e "
${CYN}============================
${YEL}
Be sure to reboot WSL after this script.
${PUR}Run as administrator Command Promt:
${YEL}
net stop LxssManager
net start LxssManager
${NC}
Installation complete!

"