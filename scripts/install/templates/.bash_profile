#
# DigitalPoetry Bash Profile
#

# COLORIZE
# Use the follow vars to easily color the terminal output.
# Ie, printf "some \033[1;31mlight red\033[0m text.."
# Or, echo -e "some \033[1;31mlight red\033[0m text.."
# Add 10 to the number between ; & m to use as a background.
# Ie, '\033[0;41m' for a red BG instad of \033[0;31m for red text.
NC='\033[0m'         # No Color
BLK='\033[0;30m'     # Black
DGRY='\033[1;30m'    # Dark Gray
RED='\033[0;31m'     # Red
LRED='\033[1;31m'    # Light Red
GRN='\033[0;32m'     # Green
LGRN='\033[1;32m'    # Light Green
BRN_ORG='\033[0;33m' # Brown/Orange
ORG='\033[1;33m'     # Orange
BLU='\033[0;34m'     # Blue
LBLU='\033[1;34m'    # Light Blue
PUR='\033[0;35m'     # Purple
LPUR='\033[1;35m'    # Light Purple
CYN='\033[0;36m'     # Cyan
LCYN='\033[1;36m'    # Light Cyan
LGRY='\033[0;37m'    # Light Gray
WHT='\033[1;37m'     # White
SGRY='\033[1;90m'    # Soft Gray
SGRN='\033[1;92m'    # Soft Green
SORG='\033[1;93m'    # Soft Orange
SPUR='\033[1;94m'    # Soft Purple
SRED='\033[1;95m'    # Soft Red
SBLU='\033[1;96m'    # Soft Blue
SWHT='\033[1;97m'    # Soft Gray


# Colorize `ls`
#
# See: https://askubuntu.com/questions/466198/how-do-i-change-the-color-for-directories-with-ls-in-the-console
#
# bd = (BLOCK, BLK)   Block device (buffered) special file
# cd = (CHAR, CHR)    Character device (unbuffered) special file
# di = (DIR)  Directory
# do = (DOOR) [Door][1]
# ex = (EXEC) Executable file (ie. has 'x' set in permissions)
# fi = (FILE) Normal file
# ln = (SYMLINK, LINK, LNK)   Symbolic link. If you set this to ‘target’ instead of a numerical value, the color is as for the file pointed to.
# mi = (MISSING)  Non-existent file pointed to by a symbolic link (visible when you type ls -l)
# no = (NORMAL, NORM) Normal (non-filename) text. Global default, although everything should be something
# or = (ORPHAN)   Symbolic link pointing to an orphaned non-existent file
# ow = (OTHER_WRITABLE)   Directory that is other-writable (o+w) and not sticky
# pi = (FIFO, PIPE)   Named pipe (fifo file)
# sg = (SETGID)   File that is setgid (g+s)
# so = (SOCK) Socket file
# st = (STICKY)   Directory with the sticky bit set (+t) and not other-writable
# su = (SETUID)   File that is setuid (u+s)
# tw = (STICKY_OTHER_WRITABLE)    Directory that is sticky and other-writable (+t,o+w)
# *.extension =   Every file using this extension e.g. *.rpm = files with the ending .rpm
#
# Effects:
# 00 Default color
# 01 Bold
# 04 Underlined
# 05 Flashing text
# 07 Reversed
# 08 Concealed
#
# BG's:
# Black  40
# Red    41
# Green  42
# Orange 43
# Blue   44
# Purple 45
# Cyan   46
# Gray   47
#
# Format:
# LS_COLORS='xy=fx;color;bg'
# LS_COLORS='ow=01;36;40'
LS_COLORS='ow=00;36;40:di=01;36;40'
export LS_COLORS


# CUSTOMIZE
# https://blog.grahampoulter.com/2011/09/show-current-git-bazaar-or-mercurial.html
# https://stackoverflow.com/questions/4133904/ps1-line-with-git-current-branch-and-colors
# https://www.ibm.com/developerworks/linux/library/l-tip-prompt/
# export PS1="[${DGRY}\j running${NC}|${DGRY}cmd:\!${NC}] ${LGRN}\u${PUR}@${LGRN}\H${LGRY}:${LCYN}\w${ORG}\$${NC} "


# Set Terminal Title
xtitle "DigitalPoetry ⚡"


# ALIASES
alias ap="alias -p"
alias cls="clear"
alias gb="cd $OLDPWD"
alias cdl="cd \"$@\" && ls -lah"
alias cds="cd \"$@\" && ls -lah"
alias cdls="cd \"$@\" && ls -lah"
alias www="cd ~/localhost/www"
alias ~="cd ~"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias ..1="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."
alias ..5="cd ../../../../.."


# UTILITIES

# Bash GIT Prompt
source .bash/git_prompt.sh
