#!/usr/bin/env bash

# Based on:
# https://github.com/riobard/bash-powerline

__gitprompt() {

    # Symbols
    readonly SYMBOL_GIT_BRANCH=''
    readonly SYMBOL_GIT_MODIFIED='✎'
    readonly SYMBOL_GIT_PUSH='⬆'
    readonly SYMBOL_GIT_PULL='⬇'
    readonly SYMBOL_PS='❖'
    readonly SYMBOL_SEPARATOR=':'

    # Colorscheme
    readonly NC='\033[0m'           # No Color
    readonly GRN='\[\033[00;32m\]'  # Green
    readonly BRED='\[\033[01;31m\]' # Bold Red
    readonly ORG='\033[00;33m'      # Orange
    readonly BORG='\033[01;33m'     # Bold Orange
    readonly BCYN='\033[01;36m'     # Bold Cyan
    readonly BBLU='\033[01;34m'     # Bold Cyan

    __git_info() {

        # GIT installed?
        hash git 2>/dev/null || return

        # Force GIT output in English
        local git_en="env LANG=C git"

        # Get current branch name
        local ref=$($git_en symbolic-ref --short HEAD 2>/dev/null)

        # Check for a reference
        if [[ -n "$ref" ]]; then

            # Prepend branch symbol
            ref="$BCYN$SYMBOL_GIT_BRANCH$BBLU[$ref]$NC"
        else

            # Get tag name or short unique hash instead
            ref=$($git_en describe --tags --always 2>/dev/null)
        fi

        # Is $ref empty
        [[ -n "$ref" ]] || return

        # GIT status marks
        local marks

        # Scan first two lines of output from `GIT status`
        while IFS= read -r line; do

            # Is header line (first line)
            if [[ $line =~ ^## ]]; then

                # Is curent branch ahead of the orgin?
                [[ $line =~ ahead\ ([0-9]+) ]] && marks+="$SYMBOL_GIT_PUSH${BASH_REMATCH[1]}"

                # Is curent branch behind the orgin?
                [[ $line =~ behind\ ([0-9]+) ]] && marks+="$SYMBOL_GIT_PULL${BASH_REMATCH[1]}"

            else
                # Output contains addition line(s).
                # Past header line, branch is modified.
                marks="$BCYN$SYMBOL_GIT_MODIFIED$BCYN$marks$NC"
                break
            fi
        # Get GIT status and loop each line
        # Note space between two <
        done < <($git_en status --porcelain --branch 2>/dev/null)

        # print the GIT branch segment without a trailing newline
        printf "$ref$marks"
    }

    ps1() {

        # Ending Segment
        # Check exit code of previous command and colorize the prompt accordingly.
        if [ $? -eq 0 ]; then
            # Success color
            local symbol="$BORG $SYMBOL_PS $NC"
        else
            # Failure color
            local symbol="$BRED $SYMBOL_PS $NC"
        fi

        # Starting Segment
        local cwd="$GRN\W$ORG$SYMBOL_SEPARATOR$NC"

        # Middle Segment
        # Bash by default expands the content of PS1 unless promptvars is disabled.
        # We must use another layer of reference to prevent expanding any user
        # provided strings, which would cause security issues.
        # POC: https://github.com/njhartwell/pw3nage
        # Related fix in git-bash: https://github.com/git/git/blob/9d77b0405ce6b471cb5ce3a904368fc25e55643d/contrib/completion/git-prompt.sh#L324
        if shopt -q promptvars; then
            __gitprompt_git_info="$(__git_info)"
            local git="\${__gitprompt_git_info}"
        else
            # promptvars is disabled. Avoid creating unnecessary env var.
            local git="$(__git_info)"
        fi

        PS1="$cwd$git$symbol"
    }

    PROMPT_COMMAND="ps1${PROMPT_COMMAND:+; $PROMPT_COMMAND}"
}

__gitprompt
unset __gitprompt