@ECHO OFF
TITLE Add A Virtualhost

ECHO NOTE: this script must be run as an administrator!!!

REM Set absolute paths for hosts / localhost.
SET HOSTS=C:\Windows\System32\drivers\etc\hosts
SET LOCAL=C:\DigitalPoetry\localhost

REM Prompt user for mode.
CHOICE /C AR /M "Add or Remove Virtualhost?"
if %ERRORLEVEL%==1 SET MODE=Add
if %ERRORLEVEL%==2 SET MODE=Remove
ECHO %MODE% A Virtualhost:

REM  Get the hostname.
SET /P HOSTNAME=" Enter a hostname: "

REM Add or remove virtualhost?
IF %MODE%==Add (

	REM Edit hosts and verIFy success by checking filesize.
	ECHO 127.0.0.1    %HOSTNAME% >> %HOSTS%

	REM Create vituralhost directories.
	SET HOSTPATH=%LOCAL%\www\%HOSTNAME%
	MKDIR %HOSTPATH%
	XCOPY %LOCAL%\skel\* %HOSTPATH% /E

	REM Output results.
	ECHO Added virturalhost %HOSTNAME%. The webbroot is:
	ECHO %HOSTPATH%
	ECHO.

) ELSE IF %MODE%==Remove (

	REM Edit hosts and verify success by checking filesize.
	FOR %%A IN (%HOSTS%) DO SET FILESIZE1=%%~zA
	TYPE %HOSTS% | FINDSTR /v %HOSTNAME% > %HOSTS%.tmp
	COPY /Y %HOSTS%.tmp %HOSTS%
	DEL /F %HOSTS%.tmp
	FOR %%A IN (%HOSTS%) DO SET FILESIZE2=%%~zA

	REM Remove vituralhost directories.
	DEL /F /Q %HOSTPATH%\*

	REM Output results.
	ECHO.
	ECHO Removed virturalhost %Hostname%.
	ECHO.
)

PAUSE