#!/bin/bash
# IMPORTANT: This script must be run as an administrator!
#   Add Site:
#       $ manage-hosts.sh add site.local
#   Remove Site:
#       $ manage-hosts.sh remove site.local


# Check the line ending style of the scipts. Stop the script if not unix style.
file install_tools.sh | grep -q CRLF && echo "This file has Windows style " \
"line endings (CRLF). Please correct the file and run it again. This script " \
"will now end." && exit


# Set vars for colorizing output
NC='\033[0m'      # No Color
DGRY='\033[1;30m' # Dark Gray
YEL='\033[1;33m'  # Yellow
PUR='\033[0;35m'  # Purple
CYN='\033[0;36m'  # Cyan
LCYN='\033[1;36m' # Light Cyan


# Instructions / Get windows user profile path
echo -e "
${CYN}============================
${LCYN}WSL LAMP Installer
${CYN}============================
${NC}
This script manages the hosts file on WSl for Windows 10.
${YEL}
NOTE: You must run this script as an administrator!!!
${NC}"

# PATH TO YOUR HOSTS FILE
HOSTS=/mnt/c/Windows/System32/drivers/etc/hosts

# DEFAULT IP FOR HOSTNAME
IP="127.0.0.1"

# Hostname to add/remove.
HOSTNAME=$2

add() {
    echo "Adding host:";
    HOSTS_LINE="$IP\t$HOSTNAME"
    if [ -n "$(grep $HOSTNAME $HOSTS)" ]
    then
        echo "$HOSTNAME already exists"
    else
        sudo -- sh -c -e "echo '$HOSTS_LINE' >> $HOSTS";

        if [ -n "$(grep $HOSTNAME $HOSTS)" ]
        then
            echo "Host $HOSTNAME was added succesfully";
            if [ ! -d "~/localhost/www/${HOSTNAME}" ];
            then
                echo Creating a default index file...
                cp -R ~/localhost/skel ~/localhost/www/${HOSTNAME}
                mv ~/localhost/www/${HOSTNAME}/site.sublime-project ~/localhost/www/${HOSTNAME}/${HOSTNAME%.*}.sublime-project
            else
                echo host directory exists already
                echo Skipping default index creation
            fi
        else
            echo "Failed to add $HOSTNAME";
            echo "Are you running as an administrator?";
        fi
    fi
}

remove() {

    # Check if the hosts file contains the hostname
    echo "Removing host:";
    if [ -n "$(grep $HOSTNAME $HOSTS)" ]
    then
        # Delete the host entry from hosts
        sudo sed -i".bak" "/$HOSTNAME/d" $HOSTS

        # Check if host entry was removed
        if [ ! -d "~/localhost/www/${HOSTNAME}" ];
        then
            # Remove the host files
            echo "Host $HOSTNAME was removed succesfully";
            echo Removing host directory.
            rm -rf ~/localhost/www/$HOSTNAME
        else
            echo host directory does not exists. No directories or files removed.
        fi

    else
        echo "Host $HOSTNAME was not found...";
    fi
}

$@