@ECHO OFF
REM IMPORTANT: This script must be run as an administrator!

REM Usage:
REM Add Site:
REM 	manage-hosts.bat add site.local
REM Remove Site:
REM 	manage-hosts.bat remove site.local

SET ACTION=%1
SET HOSTNAME=%2
SET HOSTS=C:\Windows\System32\drivers\etc\hosts
SET LOCAL=C:\DigitalPoetry\localhost

CALL :%ACTION% %HOSTNAME%
EXIT /B

:add
	REM Check if host already exists
	FINDSTR /C:%HOSTNAME% %HOSTS%
	If %ERRORLEVEL% EQU 0 ECHO Host %HOSTNAME% already exists! && ECHO Exiting... && GOTO:EOF

	REM  Set filesize before editing
	FOR %%A IN (%HOSTS%) DO SET FILESIZE1=%%~zA

	REM  Edit Windows hosts file
	ECHO 127.0.0.1	%HOSTNAME% >> %HOSTS%

	REM  Set filesize after editing
	FOR %%A IN (%HOSTS%) DO SET FILESIZE2=%%~zA

	REM Check if hosts file edit successful
	IF %FILESIZE1% == %FILESIZE2% ECHO Host creation failed && GOTO:EOF
	ECHO Added Host: %HOSTNAME%

	REM Add site files
	XCOPY /E "%LOCAL%\skel\*" "%LOCAL%\www\%HOSTNAME%\*"
	REN %LOCAL%\www\%HOSTNAME%\site.sublime-project %HOSTNAME:.local=%.sublime-project

	REM Check if file operation was successful
	IF NOT EXIST "%LOCAL%\www\%HOSTNAME%" ECHO Failed to create host files
GOTO:EOF

:remove
	REM Check if host does not exist
	FINDSTR /C:%HOSTNAME% %HOSTS%
	If %ERRORLEVEL% EQU 1 ECHO Host %HOSTNAME% does not exist! && GOTO:EOF

	REM  Set filesize before editing
	FOR %%A IN (%HOSTS%) DO SET FILESIZE1=%%~zA

	REM  Edit Windows hosts file
	TYPE %HOSTS% | FINDSTR /v %HOSTNAME% > %HOSTS%.tmp
	COPY /Y "%HOSTS%.tmp" "%HOSTS%"
	DEL /F "%HOSTS%.tmp"

	REM  Set filesize after editing
	FOR %%A IN (%HOSTS%) DO SET FILESIZE2=%%~zA

	REM Check if hosts file edit successful
	IF %FILESIZE1% == %FILESIZE2% ECHO Host removal failed && GOTO:EOF
	ECHO Removed Host: %HOSTNAME%.

	REM Remove site files
	RMDIR /S /Q "%LOCAL%\www\%HOSTNAME%"

	REM Check if file operation was successful
	IF EXIST "%LOCAL%\www\%HOSTNAME%" ECHO Failed to remove host files
GOTO:EOF