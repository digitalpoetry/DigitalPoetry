# DigitalPoetry Windows 10+ with WSL Development Enviroment

Install WSL and setup withusername `digitalpoetry`.

Clone repo Repos.
```
$ git clone https://gitlab.com/digitalpoetry/DigitalPoetry.git /mnt/c/DigitalPoetry
$ git clone https://gitlab.com/digitalpoetry/.ahk.git /mnt/c/DigitalPoetry/.ahk
$ git clone https://gitlab.com/digitalpoetry/effluence.git /mnt/c/DigitalPoetry/localhost/www/effluence.local
```

Copy MyAHK.lnk to:
$ sudo cp ".ahk/MyAHK.lnk" "/mnt/c/Users/jlare/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup"

Remove GIT repo db (if cloned):
```bash
$ sudo rm -rf /mnt/c/DigitalPoetry/.git
$ sudo rm -rf /mnt/c/DigitalPoetry/.ahk/.git
```

Download & Install this pathched version of [DejaVu Sans Mono](https://github.com/powerline/fonts/tree/master/DejaVuSansMono "DejaVu Sans Mono for Powerline").
Set terminal font and colors. Reference ~/docs/digitalpoetry-terminal-theme.md

Run installation scripts:
```bash
$ cd /mnt/c/DigitalPoetry/scripts/install
$ sudo bash install-wsl-lamp.sh
$ sudo bash install-dev-tools.sh
```

Add devtools to Windows user path:
`C:\DigitalPoetry\.devtools\bin`

Test PHPMyAdmin: `http://digitalpoetry/phpmyadmin`

Create a new site: `$ sudo bash C:/DigitalPoetry/scripts/manage-hosts.sh add effluence.local`

Test virtualhosts: `http://effluence.local`

Review your bash profile: `$ cat /mnt/c/DigitalPoetry/.bash_profile`

Review your hosts file: `$ cat /mnt/c/Windows/System32/drivers/etc/hosts`

Be sure hosts has the following 2 lines:

```
127.0.0.1   localhost digitalpoetry
127.0.0.1   effluence.local
```

Review the docs directory. Setup terminal colors, opacity & font.

# Misc
### Other PHP Versions
```bash
# Install PHP???
sudo apt install php7.3

# Set PHP verson
sudo a2dismod php7.1
sudo a2enmod php7.3
sudo service apache2 restart

# Set PHP verson persistantly
sudo update-alternatives --config php
```

### Install Windows apps, if not installed

- Sublime Text 3
- Sublimerge
- GIT for windows
- ToriseGIT
- FileZilla
- [Node.js](https://nodejs.org/en/)
- cmd.exe: npm install -g csslint
- cmd.exe: npm install -g jshint
- cmd.exe: npm install -g markdownlint-cli
- [PHP7.1 TS](https://windows.php.net/downloads/releases/php-7.1.28-Win32-VC14-x64.zip) or other version from the [downloads page]](https://windows.php.net/download/)

### TODOs

- PHPSAT
- CI
- ST3 build/build plugin
- ST3 build scripts, redo for wsl
- [SublimeOnSaveBuild](https://github.com/alexnj/SublimeOnSaveBuild/blob/master/SublimeOnSaveBuild.sublime-settings)

Unsorted documentation:
- Install [PHP](https://windows.php.net/download#php-7.1) for windows to "C:\php" and add the Windows PATH.