<?php
/**
 * Template: index.php
 *
 * Template for a Host Control Panel.
 *
 * @global string hostname Name of the virtualhost.
 * @global string root_filepath Document root of the virtualhost.
 * @global string page_title HTML title tag text.
 * @global string page_header Page header text.
 * @global string page The current page menu item ID
 * @global array menu_items Navbar Menu Items
 *
 * @package DigitalPoetry
 * @subpackage Template
 */

// Bootstrap the simple cp.
require './digitalpoetry/simple_cp.php';
