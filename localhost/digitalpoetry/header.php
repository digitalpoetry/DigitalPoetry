<?php
/**
 * Template: header.php
 *
 * Header template part. Included at the start of page templates.
 *
 * @global page_title HTML title tag text.
 * @global page_header Page header text.
 * @global page
 * @global menu_items
 *
 * @package DigitalPoetry
 * @subpackage Template
 */

?><!DOCTYPE html>
<html lang="en-US">
<head>
    <title><?php echo isset( $GLOBALS['page_title'] ) ? $GLOBALS['page_title'] : 'DigitalPoetry.info'; ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono|IBM+Plex+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="DigitalPoetry/css/starwars-glyphicons.css" rel="stylesheet">
    <style type="text/css" scoped>
        body {
            font-family: 'IBM Plex Sans', sans-serif;
            min-height: 100%;
            padding: 51px 0px;
        }
        a {
            color: #33b667;
        }
        .navbar {
            font-family: 'IBM Plex Mono', monospace;
            background-color: #3bc873;
            border-color: #33b667;
        }
        .navbar .navbar-brand {
            color: #dfdfdf;
            font-weight: bold;
        }
        #navbar > ul > li > a {
            color: #dfdfdf;
        }
        #navbar > ul > li > a:hover,
        #navbar > ul > li.active > a {
            background-color: #33b667;
            color: #fff;
        }

        .page-header, .section-header {
            font-family: 'IBM Plex Mono', monospace;
            border-bottom: 1px solid #33b667;
            padding-bottom: 9px;
            margin-top: 20px;
        }
        .page-header {
            font-size: 30px;
        }
        .section-header {
            font-size: 24px;
        }
        .table th {
            background-color: #33b770;
            color: #fff;
            font-family: 'IBM Plex Mono', monospace;
            font-size: 12px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #33b667;
        }
        .glyphicon {
            top: 2px;
        }
        .swg {
            color: #227744;
            float: left;
            margin: 11px;
        }
    </style>
</head>
<body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <i class="swg swg-2x swg-porg-7"></i>
                <a class="navbar-brand" href="http://DigitalPoetry/">DigitalPoetry</a>
            </div><!--/.nav-header -->
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php foreach ( $GLOBALS['menu_items'] as $menu_item_id => $menu_item ) : ?>
                        <li<?php echo ( $menu_item_id == $GLOBALS['page'] ) ? ' class="active"' : ''; ?>>
                            <a href="<?php echo $menu_item['href']; ?>">
                                <span class="glyphicon glyphicon-<?php echo $menu_item['icon']; ?>" aria-hidden="true"></span> <?php echo $menu_item['label']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav><!--/.navbar -->
