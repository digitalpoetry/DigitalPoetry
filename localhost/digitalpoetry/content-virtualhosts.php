<?php
/**
 * Template: content-newsite.php
 *
 * Content part template for 'newsite' page.
 *
 * @package DigitalPoetry
 * @subpackage Template
 */
?>
	<!-- Content -->
    <div class="container">

		<style type="text/css" scoped></style>

        <h1 class="page-header">Virtualhosts</h1>

		<?php
		// Open the Windows hosts file
		$file = fopen("/mnt/c/Windows/System32/drivers/etc/hosts","r");

		// Read each line
		while(! feof($file))
		{
			$line = fgets($file);
			if (!stristr($line,"digitalpoetry"))
			{
				preg_match("/^127.0.0.1\s(.+?)$/", $line, $matches);
				if (! empty($matches[1]))
				{
					echo '<a href="http://' . $matches[1] . '">' . $matches[1] . '</a>' . '<br>';
				}
			}
		}

		// Close the file pointer
		fclose($file);
		?>

        <div class="clearfix" />

    </div><!--/.container -->
