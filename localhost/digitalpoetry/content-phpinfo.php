<?php
/**
 * Template: content-newsite.php
 *
 * Content part template for 'newsite' page.
 *
 * @package DigitalPoetry
 * @subpackage Template
 */
?>
	<!-- Content -->
    <div class="container">

		<style type="text/css" scoped></style>

        <h1 class="page-header">PHP Info</h1>

		<style type="text/css">
			#phpinfo {border:none; background: #fff;font-size: 12px; font-weight: normal;}
			#phpinfo td, th, h1, h2 {font-weight: normal;}
			#phpinfo pre {margin: 0px; font-family: 'IBM Plex Mono', monospace; }
			#phpinfo a:link {color: #000099; text-decoration: none; background-color: #ffffff;}
			#phpinfo a:hover {text-decoration: underline;}
			#phpinfo table {border-collapse: collapse; width:100%; border: none;background-color: #ffffff; }
			#phpinfo .center {text-align: center; font-size: 12px; font-weight: normal;word-wrap:break-word;}
			#phpinfo .center table { text-align: left; }
			#phpinfo .center th { text-align: center !important; background: none repeat scroll 0 0 #F7F7F7;}
			#phpinfo td, th { border: 1px solid #33b667; font-size: 100%;}
			#phpinfo h1 {font-size: 20px;  padding: 20px 0 0 10px; font-family: 'IBM Plex Mono', monospace;}
			#phpinfo h2 {font-size: 18px; font-family: 'IBM Plex Mono', monospace;}
			#phpinfo .p {text-align: left;}
			#phpinfo .e {font-weight: normal; padding: 3px; background: #3bc873; border-bottom: 1px solid #33b667; width: 25%; font-family: 'IBM Plex Mono', monospace;}
			#phpinfo .h {color: #333; padding-top: 10px; background: #3bc873; font-family: 'IBM Plex Mono', monospace;}
			#phpinfo .v {background-color: #fff; color: #707070; padding: 3px;  word-wrap: break-word;}
			#phpinfo .vr {background-color: #cccccc; text-align: right; color: #000000;}
			#phpinfo img {float: left; border: 0px; padding: 5px;}
			#phpinfo hr {width: 800px; background-color: #cccccc; border: 0px; height: 1px; color: #000000;}
		</style>

		<?php
		ob_start();
		phpinfo();
		$phpinfo = ob_get_clean();
		$phpinfo = preg_replace('#^.*<body>(.*)</body>.*$#s', '$1', $phpinfo);
		echo '<div id="phpinfo">';
		echo $phpinfo;
		echo '</div>';
		?>

        <div class="clearfix" />

    </div><!--/.container -->
