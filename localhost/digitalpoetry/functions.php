<?php
/**
 * Template Functions
 *
 * @global hostname Name of the virtualhost.
 * @global root_filepath Document root of the virtualhost.
 * @global page_title Title tag text.
 * @global page_header Page header text.
 *
 * @package DigitalPoetry
 * @subpackage Template
 */

// Get Page Query Param
$GLOBALS['page'] = isset( $_GET['p'] ) ? $_GET['p'] : 'default';

// Default Host Domain
$GLOBALS['host_domain'] = isset( $GLOBALS['host_domain'] ) ? $GLOBALS['host_domain'] : 'DigitalPoetry';

// Default Page Title
$GLOBALS['page_title'] = isset( $GLOBALS['page_title'] ) ? $GLOBALS['page_title'] : 'DigitalPoetry/';

// Default Document Root
$GLOBALS['root_filepath'] = isset( $GLOBALS['root_filepath'] ) ? $GLOBALS['root_filepath'] : $_SERVER['DOCUMENT_ROOT'] . '/www';

// Header template part
function template_header() {
	require 'header.php';
}

// footer template part
function content( $part = '404' ) {
	require 'content-' . $part . '.php';
}

// footer template part
function template_footer() {
	require 'footer.php';
}
