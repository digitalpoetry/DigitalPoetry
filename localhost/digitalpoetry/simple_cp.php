<?php
/**
 * Template: simple_cp.php
 *
 * Template for a Host Control Panel.
 *
 * @global string hostname Name of the virtualhost.
 * @global string root_filepath Document root of the virtualhost.
 * @global string page_title HTML title tag text.
 * @global string page_header Page header text.
 * @global string page The current page menu item ID
 * @global array menu_items Navbar Menu Items
 *
 * @package DigitalPoetry
 * @subpackage Template
 */

// Set Template Options
$GLOBALS['menu_items'] = array(
	'default' =>      array( 'label' => 'Host Info',    'href' => '/',                'icon' => 'home' ),
	'phpmyadmin' =>   array( 'label' => 'phpMyAdmin',   'href' => '/phpmyadmin',      'icon' => 'wrench' ),
	'phpinfo' =>      array( 'label' => 'phpInfo',      'href' => '/?p=phpinfo',      'icon' => 'info-sign' ),
	'phpsysinfo' =>   array( 'label' => 'phpSysInfo',   'href' => '/phpsysinfo',      'icon' => 'stats' ),
	'virtualhosts' => array( 'label' => 'Virtualhosts', 'href' => '/?p=virtualhosts', 'icon' => 'folder-open' )
);

// Include Template Functions
require 'DigitalPoetry/functions.php';
?>

<!-- Header -->
<?php template_header(); ?>

<!-- Content -->
<?php
if ( $GLOBALS['page'] == 'default' ) :
	content('default') ;
elseif ( $GLOBALS['page'] == 'phpinfo' ) :
	content('phpinfo');
elseif ( $GLOBALS['page'] == 'virtualhosts' ) :
	content('virtualhosts');
else :
	content('404');
endif;
?>

<!-- Footer -->
<?php template_footer(); ?>
