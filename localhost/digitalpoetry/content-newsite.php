<?php
/**
 * Template: content-newsite.php
 *
 * Content part template for 'newsite' page.
 *
 * @package DigitalPoetry
 * @subpackage Template
 */
?>
	<!-- Content -->
    <div class="container">

		<style type="text/css" scoped></style>

        <h1 class="page-header"><?php echo $GLOBALS['page_header']; ?></h1>

		<p>Coming Soon!</p>

        <div class="clearfix" />

    </div><!--/.container -->
