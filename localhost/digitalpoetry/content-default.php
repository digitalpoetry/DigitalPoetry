<?php
/**
 * Template: content-host.php
 *
 * Content part template for a host index.
 *
 * @var string $GLOBALS['root_filepath'] Document root of the virtualhost.
 *
 * @package DigitalPoetry
 * @subpackage Template
 */

// Set Directories to skip when creating the Sites List
$exclude_directories = array( '.', '..', 'public', 'logs' );
// Get the url requesting this page
$request_url = 'http';
$request_url .= isset($_SERVER['HTTPS']) ? 's' : '';
$request_url .= '://';
$request_url .= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
$request_url .= $_SERVER['REQUEST_URI'] != '/' ? $_SERVER['REQUEST_URI'] : '';

// Get a list of the virtualhost's sites
$handle = opendir( $GLOBALS['root_filepath'] );
$glyphicon = '<span class="glyphicon glyphicon-globe" aria-hidden="true"></span>';
$list_sites = '';
while ( ( $file = readdir( $handle ) ) !== false )
    if ( is_dir( $GLOBALS['root_filepath'] . '/' . $file ) && !in_array( $file, $exclude_directories ) )
        $list_sites .= '<li>' . $glyphicon . '<a href="http://' . $file . '">' . $file . '</a></li>';
closedir($handle);
if ( empty( $list_sites ) )
    $list_sites = '<li>No sites found on this host.</li>';

// Get a list of the virtualhost's loaded extentions
$loaded_extensions = get_loaded_extensions();
setlocale( LC_ALL, "en_US.UTF-8" );
sort( $loaded_extensions, SORT_LOCALE_STRING );
$glyphicon = '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>';
$html_list_extens = '';
foreach ( $loaded_extensions as $extention )
    $html_list_extens .= '<li>' . $glyphicon . $extention . '</li>';
if ( empty( $html_list_extens ) )
    $html_list_extens = '<li>No extentions loaded on this host.</li>';
?>

    <!-- Content -->
    <div class="container">

        <style type="text/css" scoped>
            #sites li, #extensions li { float: left; margin: 10px 20px 0px 0px; list-style: none; }
            #sites li { min-width: 200px; }
            #extensions li { min-width: 135px; }
            #sites .glyphicon, #extensions .glyphicon { margin-right: 3px; }
        </style>

        <h1 class="page-header">Host</h1>

        <table class="table table-striped table-bordered">
            <thead>
                    <th>Request URL</th>
                    <th>Host Name</th>
                    <th>Host IP Address</th>
                    <th>Host Port Number</th>
                    <th>Host Document Root</th>
                    <th>Requester's IP Address</th>
                    <th>Requester's Port Number</th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $request_url; ?></td>
                    <td><?php echo $_SERVER['SERVER_NAME']; ?></td>
                    <td><?php echo $_SERVER['SERVER_ADDR']; ?></td>
                    <td><?php echo $_SERVER['SERVER_PORT']; ?></td>
                    <td><?php echo $_SERVER['DOCUMENT_ROOT']; ?></td>
                    <td><?php echo $_SERVER['REMOTE_ADDR']; ?></td>
                    <td><?php echo $_SERVER['REMOTE_PORT']; ?></td>
                </tr>
            </tbody>
        </table>

        <h3 class="section-header">Sites</h3>

        <ul id="sites" class="list">
            <?php echo $list_sites; ?>
        </ul>

        <div class="clearfix"></div>

        <h3 class="section-header">Extensions</h3>

        <ul id="extensions" class="list">
            <?php echo $html_list_extens; ?>
        </ul>

        <div class="clearfix" />

    </div><!--/.container -->
